import './styles/normalize.css';
import './styles/index.css';
import Header from "./components/Header/";
import AboutUs from "./components/AboutUs/";
import Feedback from "./components/Feedback/";
import Gallery from "./components/Gallery/";
import Footer from "./components/Footer/";

function App() {
    return (
      <div className = "App" >
        <Header/>
        <main class="main">
          <AboutUs/>
          <Feedback/>
          <Gallery/>
        </main>
        <Footer/>
      </div >
    );
}

export default App;