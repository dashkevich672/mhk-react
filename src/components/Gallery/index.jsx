const index = () => {
    return (
        <div class="gallery">
            <div class="gallery-wrapper">
                <h2 class="gallery__title">Customer Gallery</h2>
                <ul class="gallery__imgs">
                    <li class="gallery__imgs-item"></li>
                    <li class="gallery__imgs-item"></li>
                    <li class="gallery__imgs-item"></li>
                    <li class="gallery__imgs-item"></li>
                </ul>
                <button class="gallery__btn main_btn">View More</button>
            </div>
        </div>
    );
}

export default index;