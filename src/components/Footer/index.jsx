const index = () => {
    return (
        <footer class="footer">
        <div class="footer__wrapper">
            <div class="footer__top-bar">
                <div class="footer__short-line"></div>
                <div class="footer__logo"><svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="83.506" height="32.687" viewBox="0 0 83.506 32.687"><defs><style>{'.a{fill:none;}.b{clip-path:url(#a);}.c{fill:#fff;}.d{clip-path:url(#b);}'}</style><clipPath id="a"><path class="a" d="M0,0H28.01V32.274H0Z" transform="translate(0.126 0.205)"/></clipPath><clipPath id="b"><path class="a" d="M0,0H26.72V32.687H0Z" transform="translate(0.282 0.205)"/></clipPath></defs><g transform="translate(-0.423 -0.465)"><g transform="translate(31.14 0.26)"><path class="a" d="M0,0H28.01V32.274H0Z" transform="translate(0.126 0.205)"/><g class="b"><path class="c" d="M25.993,6.556V26.04c0,3.759-.367,4.814,2.017,5.455v.78H18.475v-.78c1.238-.458,1.1-1.7,1.1-5.455V17.283H8.481V26.04c0,3.759-.183,5,1.1,5.455v.78H0v-.78c2.429-.641,2.063-1.7,2.063-5.455V6.556C2.063,3.026,2.2,1.192,0,.78V0H9.581V.78c-1.283.412-1.1,2.246-1.1,5.776v9.811H19.575V6.556c0-3.53.184-5.364-1.1-5.776V0H28.01V.78c-2.155.412-2.017,2.246-2.017,5.776" transform="translate(0.126 0.205)"/></g></g><path class="c" d="M22.647,31.494v.78H15.4c-4.86,0-5.96-1.329-10.773-8.3C1.971,20.217,0,17.6,0,17.6L6.326,9.948S14.211.962,6.051.78V0H21.18V.78C18.108.78,14.9.871,5,13.57,15.724,27.919,16.183,28.515,16.183,28.515a7.482,7.482,0,0,0,6.464,2.98" transform="translate(61.282 0.465)"/><path class="c" d="M7.656,3.327C7.656,3.3,7.571,1,7.543.239a.25.25,0,0,0-.5,0C7,.987,6.876,3.3,6.876,3.331,6.864,4.168,6.587,14.093,0,14.093v.78H13.891v-.78c-6.069,0-6.232-9.954-6.235-10.766" transform="translate(0.423 17.866)"/><g transform="translate(1.837 0.26)"><path class="a" d="M0,0H26.72V32.687H0Z" transform="translate(0.282 0.205)"/><g class="d"><path class="c" d="M26.59,14.777a.212.212,0,0,0-.278.114l-1.562,3.04c-1.355,2.637-2.773,3.749-4.336.714v0L11.552,0H0V.78A5.614,5.614,0,0,1,5.18,3.943L18.8,32.687h.78l.732-1.825,6.4-15.813a.212.212,0,0,0-.116-.273" transform="translate(0.282 0.205)"/></g></g></g></svg></div>
                <div class="footer__short-line"></div>
            </div>
            <div class="footer__info-container">
                <div class="footer__info-block">
                    <h3 class="footer__info-title">About</h3>
                    <ul class="footer__info-list">
                        <li class="footer__info-item"><a href={"/"}>Shop</a></li>
                        <li class="footer__info-item"><a href={"/"}>Plan My Kitchen</a></li>
                        <li class="footer__info-item"><a href={"/"}>About Us</a></li>
                        <li class="footer__info-item"><a href={"/"}>Gallery</a></li>
                    </ul>
                </div>
                <div class="footer__info-block">
                    <h3 class="footer__info-title">Service</h3>
                    <ul class="footer__info-list">
                        <li class="footer__info-item"><a href={"/"}>FAQ</a></li>
                        <li class="footer__info-item"><a href={"/"}>Contact</a></li>
                        <li class="footer__info-item"><a href={"/"}>How to Buy</a></li>
                        <li class="footer__info-item"><a href={"/"}>Downloads</a></li>
                    </ul>
                </div>
                <div class="footer__info-block">
                    <h3 class="footer__info-title">Info</h3>
                    <ul class="footer__info-list">
                        <li class="footer__info-item"><a href={"/"}>Delivery</a></li>
                        <li class="footer__info-item"><a href={"/"}>Terms</a></li>
                        <li class="footer__info-item"><a href={"/"}>Privacy</a></li>
                    </ul>
                </div>
                <div class="footer__info-block">
                    <h3 class="footer__info-title">Follow</h3>
                    <ul class="footer__info-list footer__info-list-social">
                    <li class="footer__info-item footer__info-item-social">
                            <a class='footer__info-item-social-link' href={"/"}><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewbox="0 0 40 40"><defs><style>{'.round{fill:#fff;}'}</style></defs><path class="round" d="M20,40A20.005,20.005,0,0,1,12.215,1.572a20.005,20.005,0,0,1,15.57,36.856A19.875,19.875,0,0,1,20,40ZM15,17.75v3.2h2.706V29h3.3V20.949h2.567L24,17.75H21.011V15.535a1.809,1.809,0,0,1,.282-1.125,1.64,1.64,0,0,1,1.3-.421h1.688V11.14A17.528,17.528,0,0,0,21.82,11a4.093,4.093,0,0,0-2.989,1.108,4.187,4.187,0,0,0-1.125,3.111V17.75Z" transform="translate(0 0)"/></svg></a>
                        </li>
                        <li class="footer__info-item footer__info-item-social">
                            <a class='footer__info-item-social-link' href={"/"}><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewbox="0 0 40 40"><defs><style>{'.round{fill:#fff;}'}</style></defs><path class="round" d="M20,40A20.005,20.005,0,0,1,12.215,1.572a20.005,20.005,0,0,1,15.57,36.856A19.875,19.875,0,0,1,20,40ZM10,27.967h0A12.672,12.672,0,0,0,16.966,30a12.544,12.544,0,0,0,7.053-2.033,12.414,12.414,0,0,0,4.37-4.89A12.884,12.884,0,0,0,29.9,17.062a2.6,2.6,0,0,0-.043-.562,10.014,10.014,0,0,0,2.293-2.38,9.781,9.781,0,0,1-2.6.735,4.523,4.523,0,0,0,1.99-2.509,9.27,9.27,0,0,1-2.9,1.082,5.151,5.151,0,0,0-1.492-1.039,4.362,4.362,0,0,0-1.84-.39,4.418,4.418,0,0,0-2.25.606,4.474,4.474,0,0,0-1.644,1.666,4.535,4.535,0,0,0-.606,2.272,6.336,6.336,0,0,0,.086,1.039,12.706,12.706,0,0,1-9.346-4.759,4.407,4.407,0,0,0-.086,4.457,5.059,5.059,0,0,0,1.471,1.644,4.3,4.3,0,0,1-2.033-.606V18.4a4.374,4.374,0,0,0,1.039,2.856,4.6,4.6,0,0,0,2.6,1.6,5.807,5.807,0,0,1-1.211.13c-.259,0-.535-.015-.822-.043a4.537,4.537,0,0,0,1.579,2.228,4.248,4.248,0,0,0,2.661.887,9.044,9.044,0,0,1-5.668,1.948C10.68,28.01,10.317,28,10,27.967Z" transform="translate(0 0)"/></svg></a>
                        </li>
                        <li class="footer__info-item footer__info-item-social">
                            <a class='footer__info-item-social-link' href={"/"}><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewbox="0 0 40 40"><defs><style>{'.round{fill:#fff;}'}</style></defs><path class="round" d="M20,40A20.005,20.005,0,0,1,12.215,1.572a20.005,20.005,0,0,1,15.57,36.856A19.875,19.875,0,0,1,20,40Zm0-29c-1.731,0-2.995.02-3.757.06a7.672,7.672,0,0,0-1.989.341A4.706,4.706,0,0,0,11.4,14.254a7.672,7.672,0,0,0-.341,1.989C11.02,17.005,11,18.269,11,20s.02,2.995.06,3.757a7.669,7.669,0,0,0,.341,1.989,4.714,4.714,0,0,0,1.105,1.748,4.305,4.305,0,0,0,1.748,1.065,6.6,6.6,0,0,0,1.989.382c.745.04,2.009.06,3.757.06s3.011-.02,3.757-.06a7.635,7.635,0,0,0,1.989-.341A4.706,4.706,0,0,0,28.6,25.745a7.635,7.635,0,0,0,.341-1.989c.04-.745.06-2.009.06-3.757s-.014-2.993-.041-3.737a8.162,8.162,0,0,0-.361-2.009A4.706,4.706,0,0,0,25.745,11.4a7.669,7.669,0,0,0-1.989-.341C22.994,11.02,21.73,11,20,11Zm.643,16.393H19.357c-1.055,0-1.893-.013-2.491-.04a8.511,8.511,0,0,1-2.17-.322A3.062,3.062,0,0,1,12.968,25.3a8.56,8.56,0,0,1-.321-2.17c-.026-.6-.04-1.435-.04-2.491V19.357c0-1.055.013-1.893.04-2.491a8.162,8.162,0,0,1,.321-2.17A2.926,2.926,0,0,1,14.7,12.968a8.571,8.571,0,0,1,2.17-.321c.6-.026,1.436-.04,2.491-.04h1.285c1.056,0,1.894.013,2.491.04a8.152,8.152,0,0,1,2.17.321A2.918,2.918,0,0,1,27.031,14.7a8.106,8.106,0,0,1,.322,2.17c.026.57.04,1.408.04,2.491v1.285c0,1.056-.013,1.894-.04,2.491a8.5,8.5,0,0,1-.322,2.17A3.052,3.052,0,0,1,25.3,27.031a8.1,8.1,0,0,1-2.17.322C22.564,27.379,21.726,27.392,20.642,27.392ZM20,15.379a4.523,4.523,0,0,0-2.31.623A4.653,4.653,0,0,0,16,17.689a4.6,4.6,0,0,0,0,4.621A4.647,4.647,0,0,0,17.689,24a4.6,4.6,0,0,0,4.621,0A4.642,4.642,0,0,0,24,22.31a4.6,4.6,0,0,0,0-4.621A4.647,4.647,0,0,0,22.31,16,4.523,4.523,0,0,0,20,15.379Zm4.821-1.286a1.085,1.085,0,0,0,0,2.17.982.982,0,0,0,.743-.321,1.25,1.25,0,0,0,.341-.763,1.1,1.1,0,0,0-1.084-1.085ZM20,23.013a3.017,3.017,0,1,1,2.129-.884A2.915,2.915,0,0,1,20,23.013Z" transform="translate(0 0)"/></svg></a>
                        </li>
                    </ul>
                </div>
            </div>
            <p class="footer__copyright">Copyright Online MTC Home Kitchens 2018 - All rights reserved. Responsive website design, Development & Hosting by mtc.</p>
        </div>
    </footer>
    );
}

export default index;