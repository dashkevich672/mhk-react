const index = () => {
    return (
        <div class="about-us">
            <div class="about-us__banner"></div>
            <div class="about-us__info">
                <div class="about-us__info-wrapper">
                    <h3 class="subtitle">Quality Craftmanship from build to delivery</h3>
                    <h1 class="about-us__info-title">Discover the beauty of a handmade kitchen</h1>
                    <p class="about-us__info-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eget est sit amet sapien venenatis maximus vel in urna. Nam mauris arcu, feugiat in finibus vitae, sollicitudin id purus. Ut imperdiet, magna eu pharetra tincidunt, mauris
                        purus ultrices.</p>
                    <button class="about-us__info-btn main_btn">About Us</button>
                </div>
            </div>
        </div>
    );
}

export default index;