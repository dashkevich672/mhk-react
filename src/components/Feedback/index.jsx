const index = () => {
    return (
        <div class="feedback">
            <div class="feedback-info-wrapper">
                <h3 class="subtitle">What Our Customers Say</h3>
                <h2 class="feedback__title">Over 35 years experience designing handmade kitchens</h2>
                <p class="feedback__text">Since my first contact I have received a very high level of customer service and advice with my kitchen plans. Ben responded very quickly to all of my emails and delivery of the kitchen was as planned.</p>
                <span class="feedback__text-author">Jane, Dundee</span>
                <button class="feedback__btn main_btn">Frequently Asked Questions</button>
                <div class="feedback__arrow-left"></div>
                <div class="feedback__arrow-right"></div>
            </div>
        </div>
    );
}

export default index;