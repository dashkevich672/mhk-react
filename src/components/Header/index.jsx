const index = () => {
    return (
        <header class="header">
        <div class="header__banner">
            <div class="header__navigation-wrapper">
                <nav class="header__navigation">
                    <ul class="header__social">
                        <li class="header__social-item">
                            <a href={"/"}>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" fill="#ffffff" height="16" width="16" version="1.1" id="Layer_1" viewBox="0 0 310 310" xmlSpace="preserve">
                                    <g id="XMLID_834_">
                                        <path id="XMLID_835_" d="M81.703,165.106h33.981V305c0,2.762,2.238,5,5,5h57.616c2.762,0,5-2.238,5-5V165.765h39.064   c2.54,0,4.677-1.906,4.967-4.429l5.933-51.502c0.163-1.417-0.286-2.836-1.234-3.899c-0.949-1.064-2.307-1.673-3.732-1.673h-44.996   V71.978c0-9.732,5.24-14.667,15.576-14.667c1.473,0,29.42,0,29.42,0c2.762,0,5-2.239,5-5V5.037c0-2.762-2.238-5-5-5h-40.545   C187.467,0.023,186.832,0,185.896,0c-7.035,0-31.488,1.381-50.804,19.151c-21.402,19.692-18.427,43.27-17.716,47.358v37.752H81.703   c-2.762,0-5,2.238-5,5v50.844C76.703,162.867,78.941,165.106,81.703,165.106z"></path>
                                    </g>
                                </svg>
                            </a>
                        </li>
                        <li class="header__social-item">
                            <a href={"/"}>
                            <svg viewBox="328 355 335 276" xmlns="http://www.w3.org/2000/svg" fill="#ffffff" height="16" width="16">
                                    <path d="M 630, 425 A 195, 195 0 0 1 331, 600 A 142, 142 0 0 0 428, 570 A  70,  70 0 0 1 370, 523 A  70,  70 0 0 0 401, 521 A  70,  70 0 0 1 344, 455  A  70,  70 0 0 0 372, 460A  70,  70 0 0 1 354, 370A 195, 195 0 0 0 495, 442A  67,  67 0 0 1 611, 380A 117, 117 0 0 0 654, 363A  65,  65 0 0 1 623, 401A 117, 117 0 0 0 662, 390A  65,  65 0 0 1 630, 425 Z"></path>
                                </svg>
                            </a>
                        </li>
                        <li class="header__social-item">
                            <a href={"/"}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffffff">
                                    <path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.9 3.9 0 0 0-1.417.923A3.9 3.9 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.9 3.9 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.9 3.9 0 0 0-.923-1.417A3.9 3.9 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599s.453.546.598.92c.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.5 2.5 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.5 2.5 0 0 1-.92-.598 2.5 2.5 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233s.008-2.388.046-3.231c.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92s.546-.453.92-.598c.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92m-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217m0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334"/>
                                </svg>
                            </a>
                        </li>
                    </ul>
                    <ul class="header__links">
                        <li class="header__link"><a href={"/"}>shop</a></li>
                        <li class="header__link"><a href={"/"}>plan my kitchen</a></li>
                    </ul>
                    <div class="header__logo">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="83.506" height="32.687" viewBox="0 0 83.506 32.687"><defs><style>{'.a{fill:none;}.b{clip-path:url(#a);}.c{fill:#fff;}.d{clip-path:url(#b);}'}</style><clipPath id="a"><path class="a" d="M0,0H28.01V32.274H0Z" transform="translate(0.126 0.205)"/></clipPath><clipPath id="b"><path class="a" d="M0,0H26.72V32.687H0Z" transform="translate(0.282 0.205)"/></clipPath></defs><g transform="translate(-0.423 -0.465)"><g transform="translate(31.14 0.26)"><path class="a" d="M0,0H28.01V32.274H0Z" transform="translate(0.126 0.205)"/><g class="b"><path class="c" d="M25.993,6.556V26.04c0,3.759-.367,4.814,2.017,5.455v.78H18.475v-.78c1.238-.458,1.1-1.7,1.1-5.455V17.283H8.481V26.04c0,3.759-.183,5,1.1,5.455v.78H0v-.78c2.429-.641,2.063-1.7,2.063-5.455V6.556C2.063,3.026,2.2,1.192,0,.78V0H9.581V.78c-1.283.412-1.1,2.246-1.1,5.776v9.811H19.575V6.556c0-3.53.184-5.364-1.1-5.776V0H28.01V.78c-2.155.412-2.017,2.246-2.017,5.776" transform="translate(0.126 0.205)"/></g></g><path class="c" d="M22.647,31.494v.78H15.4c-4.86,0-5.96-1.329-10.773-8.3C1.971,20.217,0,17.6,0,17.6L6.326,9.948S14.211.962,6.051.78V0H21.18V.78C18.108.78,14.9.871,5,13.57,15.724,27.919,16.183,28.515,16.183,28.515a7.482,7.482,0,0,0,6.464,2.98" transform="translate(61.282 0.465)"/><path class="c" d="M7.656,3.327C7.656,3.3,7.571,1,7.543.239a.25.25,0,0,0-.5,0C7,.987,6.876,3.3,6.876,3.331,6.864,4.168,6.587,14.093,0,14.093v.78H13.891v-.78c-6.069,0-6.232-9.954-6.235-10.766" transform="translate(0.423 17.866)"/><g transform="translate(1.837 0.26)"><path class="a" d="M0,0H26.72V32.687H0Z" transform="translate(0.282 0.205)"/><g class="d"><path class="c" d="M26.59,14.777a.212.212,0,0,0-.278.114l-1.562,3.04c-1.355,2.637-2.773,3.749-4.336.714v0L11.552,0H0V.78A5.614,5.614,0,0,1,5.18,3.943L18.8,32.687h.78l.732-1.825,6.4-15.813a.212.212,0,0,0-.116-.273" transform="translate(0.282 0.205)"/></g></g></g></svg>
                    </div>
                    <ul class="header__links">
                        <li class="header__link"><a href={"/"}>about us</a></li>
                        <li class="header__link"><a href={"/"}>gallery</a></li>
                    </ul>
                    <button class="header__order-btn">my order</button>
                </nav>
            </div>
            <div class="header__titles">
                <h3 class="header__titles-subtitle">Design and order your new kitchen online today</h3>
                <h1 class="header__titles-maintitle">Bespoke & made to measure handmade kitchen design</h1>
                <button class="header__titles-order-btn main_btn">order now</button>
            </div>
            <div class="header__sliderbar">
                <div class="header__sliderbar-bar active"></div>
                <div class="header__sliderbar-bar"></div>
                <div class="header__sliderbar-bar"></div>
            </div>
        </div>
    </header>
    );
}

export default index;